package net.portic.tiazuqueca;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import net.portic.tiazuqueca.dao.TrackingAzuquecaJdbcTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class AzuquecaTrackingImpl {

	private final static String NIF_TERMINAL_AZUQUECA = "ESA19145192";

	private final static Logger log = Logger.getLogger(AzuquecaTrackingImpl.class);

	@Autowired
	private TrackingAzuquecaJdbcTemplate tiaDao;

	@Path("/tracking/consultar")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Movimiento> consultarTracking(QueryParams params) {
		List<Movimiento> result = new ArrayList<Movimiento>();
		try {

			String nifTerminal = params.getNifTerminal();
			if (nifTerminal == null || "".equals(nifTerminal)) {
				nifTerminal = NIF_TERMINAL_AZUQUECA;
			}
			String equipmentId = params.getMatriculaEquipo();
			Date inicio = params.getInicio();
			Date fin = params.getFin();
			result = tiaDao.queryTracking(nifTerminal, equipmentId, inicio, fin);
		} catch (Exception e) {
			log.error("Oops! Something went wrong with consultarTracking", e);
		} finally {
		}
		return result;
	}


	@Path("/tracking/ping")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String ping() {
		Long result = -1L;
		try {
		} catch (Exception e) {
			log.error("Oops! Something went wrong with consultarTracking", e);
		} finally {
		}
		return "pong!";
	}
}
