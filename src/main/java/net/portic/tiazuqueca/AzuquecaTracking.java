package net.portic.tiazuqueca;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface AzuquecaTracking {
    public Long consultarTracking(String payload);
}

