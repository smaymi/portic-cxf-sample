package net.portic.tiazuqueca;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Created by smaymi on 15/12/2015.
 */
@XmlRootElement
public class QueryParams {
    String nifTerminal;
    String matriculaEquipo;
    Date inicio;
    Date fin;

    public String getNifTerminal() {
        return nifTerminal;
    }

    public void setNifTerminal(String nifTerminal) {
        this.nifTerminal = nifTerminal;
    }

    public String getMatriculaEquipo() {
        return matriculaEquipo;
    }

    public void setMatriculaEquipo(String matriculaEquipo) {
        this.matriculaEquipo = matriculaEquipo;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }
}
