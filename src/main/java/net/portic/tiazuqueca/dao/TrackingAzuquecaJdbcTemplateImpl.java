package net.portic.tiazuqueca.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import net.portic.tiazuqueca.Movimiento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Created by smaymi on 27/02/2015.
 */
@Repository
public class TrackingAzuquecaJdbcTemplateImpl implements TrackingAzuquecaJdbcTemplate {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Movimiento> queryTracking(String nifTerminal, String matriculaEquipo, Date inicio, Date fin) {
        Long id = -1L;

        String query = "Select 'TREN' as medio,\n" +
                "       Te.Identificador as matricula, \n" +
                "       Decode(Te.Lleno_Vacio, 4, 'Vacio',5,'Lleno') as situacion, \n" +
                "       Te.Estado as estado, \n" +
                "       Decode(Te.Estado,'P',Nvl(Tr.Llegada_Real,Tr.Llegada_Prevista),'C',Te.Fecha_Movimiento) As Fecha_Movimiento, \n" +
                "       Tr.Operacion as tipo, \n" +
                "       Tr.Id_Tren As Id_Mov \n" +
                "From Portic.Ti_Trenes_Equipos Te, Portic.Ti_Trenes Tr \n" +
                "Where Tr.Id_Tren = Te.Id_Tren\n" +
                "  And Tr.EMPRESA = ? \n" +
                "  And Te.Identificador = ? \n" +
                "  And Te.Estado in ('P','C') \n" +
                "  And (Tr.Llegada_Real Is Null Or (Tr.Llegada_Real >= ? and Tr.Llegada_Real <= ?))\n" +
                "  And (Tr.Llegada_Prevista Is Null Or (Tr.Llegada_Prevista >= ? and Tr.Llegada_Prevista <= ?))\n" +
                "  And (Te.Fecha_Movimiento Is Null Or (Te.Fecha_Movimiento >= ? and Te.Fecha_Movimiento <= ?))\n" +
                "UNION\n" +
                "Select 'CAMION' as medio, Cm.Identificador as matricula, Decode(Cm.Lleno_Vacio, 4, 'Vacio',5,'Lleno') as situacion, Cm.Estado as estado,  \n" +
                "       DECODE(Cm.Estado,'P',Cm.Fecha_Prevista,'C',Cm.Fecha_Inicio,'F',Cm.Fecha_Fin) As Fecha_Movimiento,  Cm.Tipo_Es  as tipo, Cm.Id_Mov As Id_Mov \n" +
                "From Portic.Ti_Camiones_Movimientos Cm \n" +
                "Where Cm.EMPRESA=? \n" +
                "  and Cm.Identificador=? \n" +
                "  and cm.estado in ('P','C','F')\n" +
                "  And (Cm.Fecha_Fin Is Null Or (Cm.Fecha_Fin >= ? and Cm.Fecha_Fin >= ?))\n" +
                "  And (Cm.Fecha_Prevista Is Null Or (Cm.Fecha_Prevista >= ? and Cm.Fecha_Prevista <= ?))\n" +
                "  And (Cm.Fecha_Inicio Is Null Or (Cm.Fecha_Inicio >= ? and Cm.Fecha_Inicio >= ?))\n" +
                "order by 5\n";
        List<Map<String,Object>> rows = jdbcTemplate.queryForList(query, new Object[] {
            nifTerminal, matriculaEquipo, inicio, fin, inicio, fin, inicio, fin,
            nifTerminal, matriculaEquipo, inicio, fin, inicio, fin, inicio, fin,
        });
        List<Movimiento> result = new ArrayList<Movimiento>();
        for (Map<String,Object> row : rows) {
            Movimiento movimiento = new Movimiento();
            movimiento.setEstado(String.valueOf(row.get("estado")));
            movimiento.setSituacion(String.valueOf(row.get("situacion")));
            movimiento.setTipo(String.valueOf(row.get("tipo")));
            movimiento.setMedio(String.valueOf(row.get("madio")));
            Timestamp ts = (Timestamp)row.get("Fecha_Movimiento");
            movimiento.setFecha(new Date(ts.getTime()));
            result.add(movimiento);
        }
        return result;
    }


}