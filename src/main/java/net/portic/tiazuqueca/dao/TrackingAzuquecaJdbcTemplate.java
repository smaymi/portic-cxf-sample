package net.portic.tiazuqueca.dao;

import net.portic.tiazuqueca.Movimiento;

import java.util.Date;
import java.util.List;

/**
 * Created by smaymi on 28/02/2015.
 */
public interface TrackingAzuquecaJdbcTemplate {
    public List<Movimiento> queryTracking(String nifTerminal, String equipment, Date initDate, Date endDate);

}
